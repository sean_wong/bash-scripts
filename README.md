# Bash Scripts

Collection of simple shell scripts with varying degrees of utility

1. [setbat.sh](scripts/setbat.sh): display battery charge and icon depending on battery status
2. [setbright.sh](scripts/setbright.sh): get brightness percentage
3. [setcmus.sh](scripts/setcmus.sh): get song artist, song title. song status from cmus
4. [setvol.sh](scripts/setvol.sh): print volume and mute status
5. [setclock.sh](scripts/setclock.sh): print time and date
6. [emoji.sh](scripts/emoji.sh): copy emoji to clipboard based on description
7. [mount.sh](scripts/mount.sh): list mountable drives and mounts choice
8. [unmount.sh](scripts/unmount.sh): list mounted drives and unmounts choice
