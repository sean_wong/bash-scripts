#!/bin/sh

mountable="$(lsblk -nrpo "name,rm,size,type,mountpoint" | grep "part" | awk '$5==""{printf "%s (%s)\n",$1,$3}')"

to_mount="$(echo "$mountable" | dmenu -fn "System San Francisco Display
Regular:size=10" -nb "#282828" -nf "#a89984" -sb "#83a598" -sf "#282828" -i -p "Mount which drive?" | awk '{print $1}')" || exit 1

mount $to_mount
