#!/bin/sh

mute=`amixer sget Master | grep 'Mono:' | awk -F"[][]" '{print $6}'`
vol=`amixer sget Master | grep 'Mono:' | awk -F"[][]" '{ print $2 }'`

if [ "$mute" = "off" ]; then
	echo "  $vol "
else
	echo "  $vol "
fi
