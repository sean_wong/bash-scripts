#!/bin/sh

unmountable="$(lsblk -npro "name,rm,mountpoint" | grep "/media/jasmine/" | awk '{printf "%s @ %s\n",$1,$3}')"

to_unmount="$(echo "$unmountable" | dmenu -fn "System San Francisco Display Regular:size=10" -nb "#282828" -nf "#a89984" -sb "#83a598" -sf "#282828" -i -p "Unmount which drive?" | awk '{print $3}')" || exit 1

umount $to_unmount
