#!/bin/sh

running=`pgrep cmus`

if [ "$running" != "" ]; then
	status=`cmus-remote -Q | head -n 1 | awk '{ print $2 }'`
	artist=`cmus-remote -Q | grep "tag artist" | cut -f3- -d ' '`
	title=`cmus-remote -Q | grep "tag title" | cut -f3- -d ' '`

	if [ "$status" = "playing" ]; then
		echo "  $artist - $title "
	else
		echo "  $artist - $title "
	fi
else
	echo ""
fi
